
//Select the section with an id of container without using querySelector.
let a = document.getElementById("container");
console.log(a);

// Select the section with an id of container using querySelector.
let b =document.querySelector("#container");
console.log(b);

//Select all of the list items with a class of "second".
let c = document.getElementsByClassName("second");
console.log(c);

//select a list item with a class of third, but only the list item inside of the ol tag.
let d = document.querySelector("ol .third");
console.log(d);

//Give the section with an id of container the text "Hello!".
let addingtext = document.getElementById("container");
addingtext.innerHTML+="Hello!";

//Before adding class
console.log(document.getElementsByClassName("footer"));
//After adding class
//Add the class main to the div with a class of footer.
let footer = document.querySelector(".footer");
footer.classList.add("main");
console.log(document.getElementsByClassName("footer"));


//Remove the class main on the div with a class of footer.
let footer1 = document.querySelector(".footer");
footer.classList.remove("main");
console.log(document.getElementsByClassName("footer"));


//Create a new li element.
let newList = document.createElement("li");

//Adding content to the list;
newList.innerText="four"


//Append the li to the ul element.
let list = document.querySelector("ul")
list.appendChild(newList);


//Loop over all of the lis inside the ol tag and give them a background color of "green".
let insideol = document.querySelectorAll('ol li');
for(let i=0;i<insideol.length;i++){
    insideol[i].style.backgroundColor="green";
    insideol[i].style.marginBottom="3px";
}

//Remove the div with a class of footer. 
var foot = document.querySelector(".footer");
foot.remove();
console.log(document);












